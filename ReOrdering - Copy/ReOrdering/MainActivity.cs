﻿using Android.App;
using Android.Widget;
using Android.OS;
using System.Collections.Generic;
using Android.Views;
using Android.Graphics;

namespace ReOrdering
{
    [Activity(Label = "ReOrdering", MainLauncher = true)]
    public class MainActivity : Activity
    {
        List<string> items;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            var list = FindViewById<DraggableListView>(Resource.Id.listview1);


            items = new List<string> {
                "Vegetables",
                "Fruits",
                "Flower Buds",
                "Legumes",
                "Vegetables",
                "Fruits",
                "Flower Buds",
                "Legumes",
                "Vegetables",
                "Fruits",
                "Flower Buds",
                "Legumes",
                "Vegetables",
                "Fruits",
                "Flower Buds",
                "Legumes",
                "Vegetables",
                "Fruits",
                "Flower Buds",
                "Legumes",
                "Vegetables",
                "Fruits",
                "Flower Buds",
                "Legumes",
                "Vegetables",
                "Fruits",
                "Flower Buds",
                "Legumes",
                "Vegetables",
                "Fruits",
                "Flower Buds",
                "Legumes",
            };
            list.Adapter = new DraggableListAdapter(this, items);
        }
    }

    public class DraggableListAdapter : BaseAdapter, IDraggableListAdapter
    {
        public List<string> Items { get; set; }


        public int mMobileCellPosition { get; set; }

        Activity context;

        public DraggableListAdapter(Activity context, List<string> items) : base()
        {
            Items = items;
            this.context = context;
            mMobileCellPosition = int.MinValue;
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return Items[position];
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View cell = convertView;
            if (cell == null)
            {
                cell = context.LayoutInflater.Inflate(Resource.Layout.Sample, parent, false);
                cell.SetMinimumHeight(150);
                cell.SetBackgroundColor(Color.DarkViolet);
            }

            var text = cell.FindViewById<TextView>(Resource.Id.sampleText);
            if (text != null)
            {
                text.Text = position.ToString();
            }

            cell.Visibility = mMobileCellPosition == position ? ViewStates.Invisible : ViewStates.Visible;
            cell.TranslationY = 0;

            return cell;
        }

        public override int Count
        {
            get
            {
                return Items.Count;
            }
        }

        public void SwapItems(int indexone, int indextwo)
        {
            var oldvalue = Items[indexone];
            Items[indexone] = Items[indextwo];
            Items[indextwo] = oldvalue;
            mMobileCellPosition = indextwo;
            NotifyDataSetChanged();
        }

    }
}

